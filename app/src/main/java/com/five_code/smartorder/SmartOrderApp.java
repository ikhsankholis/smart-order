package com.five_code.smartorder;

import android.app.Application;
import android.content.Context;

import com.five_code.smartorder.injection.component.ApplicationComponent;
import com.five_code.smartorder.injection.component.DaggerApplicationComponent;
import com.five_code.smartorder.injection.module.ApplicationModule;
import com.orhanobut.hawk.Hawk;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

public class SmartOrderApp extends Application {

    private static ApplicationComponent mApplicationComponent;

    public static SmartOrderApp get(Context context) {
        return (SmartOrderApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        setupHawk();
    }

    private void setupHawk() {
        Hawk.init(this).build();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
