package com.five_code.smartorder.utils;

import com.five_code.smartorder.base.MvpView;
import com.five_code.smartorder.data.model.BaseResponse;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

public class ErrorHandler {
    public static void handlerErrorPresenter(MvpView mvpView, Throwable throwable) {
        String message = ErrorHandler.handleError(throwable);
        mvpView.onFailed(message);
    }

    public static String handleError(Throwable throwable) {
        Logger.log(throwable);

        if (throwable == null) return "Please check your network and try again.";
        if (throwable instanceof AppException) {
            AppException appException = (AppException) throwable;
            BaseResponse baseResponse = appException.getResponse();

            if (baseResponse != null) {
                String message = baseResponse.getMeta().getMessage();
                if (!message.isEmpty()) {
                    return message;
                }
            }
        }

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            if (httpException.code() == 403) {
                forceLogout();
                return null;
            }
        }
        return "Please check your network and try again.";
    }

    private static void forceLogout() {
    }
}
