package com.five_code.smartorder.utils;

import android.os.Looper;
import android.util.Log;

import com.five_code.smartorder.data.model.BaseResponse;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

public class RxUtils {

    public static <T> Observable.Transformer<T, T> applyScheduler() {
        return tObservable -> tObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> Observable.Transformer<T, T> applySchedulersNewThread() {
        return observable -> observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> Observable.Transformer<T, T> applySchedulersComputation() {
        return observable -> observable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T extends BaseResponse> Observable.Transformer<T, T> applyResponseErrorHandler() {
        return observable -> observable.flatMap(res -> {
            if (res == null) return Observable.error(new NullPointerException());
            if (res.getMeta().isStatus()) {
                return Observable.just(res);
            }
            return Observable.error(new AppException(res));
        });
    }

    public static <T> Observable.Transformer<T, T> applyApiCall() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {

                    Logger.log(throwable);
                    if (throwable instanceof HttpException) {
                        try {
                            HttpException httpException = (HttpException) throwable;
                            String res = httpException.response().errorBody().string();

                            Gson gson = new Gson();
                            BaseResponse baseResponse = gson.fromJson(res, BaseResponse.class);
                            AppException e = new AppException(baseResponse, httpException.response().code());
                            return Observable.error(e);
                        } catch (IOException e) {
                            return Observable.empty();
                        }
                    } else if (throwable instanceof RetrofitException) {
                        try {
                            RetrofitException retrofitException = (RetrofitException) throwable;
                            String res = retrofitException.getResponse().errorBody().string();

                            Gson gson = new Gson();
                            BaseResponse baseResponse = gson.fromJson(res, BaseResponse.class);
                            AppException e =
                                    new AppException(baseResponse, retrofitException.getResponse().code());
                            return Observable.error(e);
                        } catch (IOException e) {
                            return Observable.empty();
                        }
                    } else
                        return Observable.empty();
                });
    }

    public static void checkMainThread() {
        boolean isMainThread = Looper.myLooper() == Looper.getMainLooper();
        Logger.log(Log.DEBUG, "RX___ Is main thread :" + isMainThread);
    }
}
