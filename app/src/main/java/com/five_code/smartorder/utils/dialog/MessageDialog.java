package com.five_code.smartorder.utils.dialog;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.five_code.smartorder.R;

/**
 * Created by Okta Dwi Priatna on 5/31/17.
 * okta.dwi1@gmail.com
 */

public class MessageDialog {

    public static void showMessage(Context context, int message) {
        showMessage(context, context.getString(message));
    }

    public static void showMessage(Context context, String message) {
        if (message.isEmpty()) return;

        new MaterialDialog.Builder(context)
                .content(message)
                .positiveText("OK")
                .show();
    }

    public static void showMessageTitle(Context context, String title, String message) {
        if (message.isEmpty()) return;

        new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .positiveText("OK")
                .show();
    }
}

