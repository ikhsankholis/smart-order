package com.five_code.smartorder.utils.customview;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.five_code.smartorder.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CustomToolbarAct extends RelativeLayout {

    @BindView(R.id.titlebar_title) TextView mTxtTitle;
//    @BindView(R.id.titlebar_action_button_back) ImageView mActionButtonBack;

    public CustomToolbarAct(Context context) {
        this(context, null);
    }

    public CustomToolbarAct(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbarAct(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.custom_toolbar_activity, this, true);
        if (isInEditMode())
            return;
        ButterKnife.bind(this);
    }

    public void setTitle(@StringRes int resId) {
        mTxtTitle.setText(resId);
    }

    public void setTitle(String title) {
        mTxtTitle.setText(title);
    }

//    public void setActionButtonBackClick(OnClickListener onActionButtonClick) {
//        mActionButtonBack.setOnClickListener(onActionButtonClick);
//    }
//
//    public void setLeftImage(int image){
//        mActionButtonBack.setImageResource(image);
//    }

}
