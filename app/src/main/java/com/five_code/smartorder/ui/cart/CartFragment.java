package com.five_code.smartorder.ui.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseFragment;

import butterknife.BindView;

/**
 * Created by IKHSAN on 5/18/2017.
 */

public class CartFragment extends BaseFragment {

    @BindView(R.id.rv_cart) RecyclerView rvCart;
    @BindView(R.id.lin_price) LinearLayout linPrice;
    @BindView(R.id.text_price) TextView textPrice;

    private CartAdapter cartAdapter;

    @Override
    protected int getLayout() {
        return R.layout.fragment_cart;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cartAdapter = new CartAdapter(mActivity);
        rvCart.setLayoutManager(new LinearLayoutManager(mActivity));
        rvCart.setAdapter(cartAdapter);


    }
}