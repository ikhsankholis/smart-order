package com.five_code.smartorder.ui.menu;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseFragment;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.ui.home.HomeView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class MenuFragment extends BaseFragment {

    @BindView(R.id.rv_content) RecyclerView rvContent;

    private MenuAdapter menuAdapter;

    private boolean minuman = false;
    private List<Menu> menuList = new ArrayList<>();

    private int position;

    private HomeView mHomeView;

    public static MenuFragment newInstance(boolean minuman) {
        Bundle args = new Bundle();

        MenuFragment fragment = new MenuFragment();
        args.putBoolean("kategori", minuman);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHomeView = (HomeView) activity;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_menu;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        menuAdapter = new MenuAdapter(mActivity);
        rvContent.setLayoutManager(new GridLayoutManager(mActivity, 3));
        rvContent.setAdapter(menuAdapter);

        position = getArguments().getInt("position", 0);

        menuAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final Dialog dialog = new Dialog(mActivity);
                dialog.setContentView(R.layout.dialog_add_cart);
                dialog.setCancelable(false);
                dialog.setTitle(menuAdapter.getData().get(position).getNama());

                Menu menu = menuAdapter.getData().get(position);

                ImageView imgMenu = (ImageView) dialog.findViewById(R.id.img_menu);
                EditText inpQty = ButterKnife.findById(dialog, R.id.edit_txt_qty);
                EditText inpNote = ButterKnife.findById(dialog, R.id.edit_txt_note);

                Picasso.with(getActivity()).load(menuList.get(position).getGambar()).into(imgMenu);

                ButterKnife.findById(dialog, R.id.btn_dialog_cancel).setOnClickListener(view1 -> {
                    dialog.cancel();
                });

                ButterKnife.findById(dialog, R.id.btn_dialog_ok).setOnClickListener(view1 -> {
                    String qty = inpQty.getText().toString();
                    String note = inpNote.getText().toString();

                    if (qty.isEmpty())
                        return;

                    menu.setQuantity(Integer.parseInt(qty));
                    menu.setNote(note);

                    mHomeView.onAddCart(menu);
                    dialog.cancel();
                });

                dialog.show();
            }
        });

        minuman = getArguments().getBoolean("kategori");

        if (minuman)
            initDataDrink();
        else
            initDataFood();

        menuAdapter.addAll(menuList);
    }

    public void initDataFood(){
        menuList.add(new Menu("Kebab", "15000", "Makanan Turki", R.mipmap.kebab));
        menuList.add(new Menu("Nasi Goreng", "15000", "Makanan Turki", R.mipmap.nasi_goreng));
        menuList.add(new Menu("Bakso", "15000", "Makanan Turki", R.mipmap.baso));
        menuList.add(new Menu("Siomay", "15000", "Makanan Turki", R.mipmap.siomay));
        menuList.add(new Menu("Batagor", "Rp. 15.000", "Makanan Turki", R.mipmap.batagor));
        menuList.add(new Menu("Nasi Lemak", "Rp. 15.000", "Makanan Turki", R.mipmap.nasi_lemak));
        menuList.add(new Menu("Ayam Penyet", "Rp. 15.000", "Makanan Turki", R.mipmap.ayam_penyet));
        menuList.add(new Menu("Mie Goreng", "Rp. 15.000", "Makanan Turki", R.mipmap.mie_goreng));
    }

    public void initDataDrink(){
        menuList.add(new Menu("Es Jeruk", "Rp. 15.000", "Makanan Turki", R.mipmap.es_jeruk));
        menuList.add(new Menu("Lemon Tea", "Rp. 15.000", "Makanan Turki", R.mipmap.lemon_tea));
        menuList.add(new Menu("Cappucino", "Rp. 15.000", "Makanan Turki", R.mipmap.cappucino));
        menuList.add(new Menu("Moccacino", "Rp. 15.000", "Makanan Turki", R.mipmap.moccacino));
        menuList.add(new Menu("Green Tea", "Rp. 15.000", "Makanan Turki", R.mipmap.green_tea));
        menuList.add(new Menu("Es Cincau", "Rp. 15.000", "Makanan Turki", R.mipmap.cincau));
        menuList.add(new Menu("Teh Manis", "Rp. 15.000", "Makanan Turki", R.mipmap.teh_manis));
        menuList.add(new Menu("Kopi Hitam", "Rp. 15.000", "Makanan Turki", R.mipmap.kopi));
    }
}