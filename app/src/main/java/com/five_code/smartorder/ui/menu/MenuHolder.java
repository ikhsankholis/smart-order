package com.five_code.smartorder.ui.menu;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseItemRecyclerViewHolder;
import com.five_code.smartorder.data.model.Menu;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class MenuHolder extends BaseItemRecyclerViewHolder<Menu> {

    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @BindView(R.id.txt_nama)
    TextView txtMenu;
    @BindView(R.id.txt_harga)
    TextView txtHarga;
    @BindView(R.id.txt_stock)
    TextView txtStock;

    private int position;
    private List<Menu> menuList = new ArrayList<>();

    public MenuHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
    }

    @Override
    public void bind(Menu menu) {
        Picasso.with(mContext)
                .load(menu.getGambar())
                .into(imgMenu);
        txtMenu.setText(menu.getNama());
        txtHarga.setText(menu.getHarga());
        txtStock.setText(menu.getStock());
    }

}
