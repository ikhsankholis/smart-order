package com.five_code.smartorder.ui.cart;

import android.content.Context;
import android.view.View;

import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseItemRecyclerViewHolder;

/**
 * Created by IKHSAN on 5/18/2017.
 */

public class CartHolder extends BaseItemRecyclerViewHolder {


    public CartHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
    }

    @Override
    public void bind(Object o) {

    }
}
