package com.five_code.smartorder.ui.home;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseActivity;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.ui.menu.MenuFragment;
import com.five_code.smartorder.utils.customview.CustomToolbarAct;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class HomeAct extends BaseActivity implements HomeView {

    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.toolbar) CustomToolbarAct mCustomToolbarAct;
    @BindView(R.id.lin_cart) LinearLayout linCart;

    private List<Menu> menuCart = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mCustomToolbarAct.setTitle("SMART ORDER");

        setupViewpager();
    }

    private void showDataCart() {
        linCart.removeAllViews();
        int totalPrice = 0;
        for (Menu menu : menuCart) {
            View viewCart = getLayoutInflater().inflate(R.layout.item_cart, null);
            TextView textName = ButterKnife.findById(viewCart, R.id.txt_nama_pesanan);
            TextView textQty = ButterKnife.findById(viewCart, R.id.txt_qty);
            TextView textPrice = ButterKnife.findById(viewCart, R.id.txt_harga);
            TextView textNote = ButterKnife.findById(viewCart, R.id.txt_note);

            textName.setText(menu.getNama());
            textQty.setText(menu.getQuantity() + " pcs");
            textNote.setText(menu.getNote());

            int totalMenuPrice = menu.getQuantity() * Integer.parseInt(menu.getHarga());
            totalPrice = totalPrice + totalMenuPrice;

            textPrice.setText(totalMenuPrice + "");

            linCart.addView(viewCart);
        }
    }

    public void setupViewpager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(MenuFragment.newInstance(false), "Food");
        adapter.addFragment(MenuFragment.newInstance(true), "Drink");

        class ViewPagerAdapter extends FragmentPagerAdapter {
            private final List<Fragment> mFragmentList = new ArrayList<>();
            private final List<String> mFragmentTitleList = new ArrayList<>();

            public ViewPagerAdapter(FragmentManager manager) {
                super(manager);
            }

            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }

            public void addFragment(Fragment fragment, String title) {
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mFragmentTitleList.get(position);
            }
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onAddCart(Menu menu) {
        menuCart.add(menu);
        showDataCart();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

}
