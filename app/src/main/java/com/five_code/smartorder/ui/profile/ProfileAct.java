package com.five_code.smartorder.ui.profile;

import android.content.Intent;
import android.os.Bundle;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseActivity;
import com.five_code.smartorder.ui.home.HomeAct;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ikhsan on 15/12/17.
 */

public class ProfileAct extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.txt_home) void onClickHome(){
        startActivity(new Intent(this, HomeAct.class));
    }
}
