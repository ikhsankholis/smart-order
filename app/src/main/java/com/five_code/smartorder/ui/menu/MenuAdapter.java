package com.five_code.smartorder.ui.menu;

import android.content.Context;
import android.view.ViewGroup;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.data.model.Menu;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class MenuAdapter extends BaseAdapterItemReyclerView<Menu, MenuHolder> {
    public MenuAdapter(Context context) {

        super(context);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {

        return R.layout.item_list_menu;
    }

    @Override
    public MenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuHolder(context, getView(parent, viewType),itemClickListener, longItemClickListener);
    }
}
