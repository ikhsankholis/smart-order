package com.five_code.smartorder.ui.home;

import com.five_code.smartorder.data.model.Menu;

/**
 * Created by Okta Dwi Priatna on 12/16/17.
 * okta.dwi1@gmail.com
 */

public interface HomeView {

    void onAddCart(Menu menu);
}
