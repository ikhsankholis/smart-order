package com.five_code.smartorder.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.five_code.smartorder.SmartOrderApp;
import com.five_code.smartorder.injection.component.ConfigPersistentComponent;
import com.five_code.smartorder.injection.component.DaggerConfigPersistentComponent;
import com.five_code.smartorder.injection.component.FragmentComponent;
import com.five_code.smartorder.injection.module.FragmentModule;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import butterknife.ButterKnife;

/**
 * Created by Okta Dwi Priatna on 5/16/17.
 * okta.dwi1@gmail.com
 */

public abstract class BaseFragment extends Fragment {

    private static final String KEY_FRAGMENT_ID = "KEY_FRAGMENT_ID";
    private static final AtomicLong NEXT_ID = new AtomicLong(0);
    private static final Map<Long, ConfigPersistentComponent> sComponentsMap = new HashMap<>();

    protected Activity mActivity;
    private FragmentComponent mFragmentComponent;
    private long mFragmentId;
    protected LayoutInflater mInflater;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentId = savedInstanceState != null ? savedInstanceState.getLong(KEY_FRAGMENT_ID)
                : NEXT_ID.getAndIncrement();
        ConfigPersistentComponent configPersistentComponent;
        if (!sComponentsMap.containsKey(mFragmentId)) {
            configPersistentComponent = DaggerConfigPersistentComponent.builder()
                    .applicationComponent(SmartOrderApp.get(getActivity()).getComponent())
                    .build();
            sComponentsMap.put(mFragmentId, configPersistentComponent);
        } else {
            configPersistentComponent = sComponentsMap.get(mFragmentId);
        }
        mFragmentComponent =
                configPersistentComponent.fragmentComponent(new FragmentModule(getActivity()));
        mActivity = getActivity();

        mInflater = LayoutInflater.from(mActivity);
    }

    public FragmentComponent getFragmentComponent() {
        return mFragmentComponent;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    protected abstract
    @LayoutRes
    int getLayout();

    @Override
    public void onDestroy() {
        sComponentsMap.remove(mFragmentId);
        super.onDestroy();
    }
}