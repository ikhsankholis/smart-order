package com.five_code.smartorder.injection.component;

import com.five_code.smartorder.injection.PerActivity;
import com.five_code.smartorder.injection.module.ActivityModule;

import dagger.Subcomponent;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

@PerActivity
@Subcomponent(modules = { ActivityModule.class})
public interface ActivityComponent {
}
