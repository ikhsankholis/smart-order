package com.five_code.smartorder.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Okta Dwi Priatna on 12/8/17.
 * okta.dwi1@gmail.com
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "siswa";

    private static final String TABLE_SISWA = "table_siswa";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "nama";
    private static final String KEY_HARGA = "harga";
    private static final String KEY_QUANTITY = "quantity";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_SISWA_TABLE = "CREATE TABLE " + TABLE_SISWA + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_HARGA + " TEXT,"
                + KEY_QUANTITY + " INTEGER" + ")";
        db.execSQL(CREATE_SISWA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SISWA);

        onCreate(db);
    }

//    public void addSiswa(User user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, user.get_name()); // Name
//        values.put(KEY_EMAIL, user.get_email()); // Email
//        values.put(KEY_PASSWORD, user.get_password()); // Password
//
//        // Inserting Row
//        db.insert(TABLE_SISWA, null, values);
//        db.close(); // Closing database connection
//    }
//
//    User getSiswa(int id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_SISWA, new String[]{KEY_ID,
//                        KEY_NAME, KEY_EMAIL}, KEY_ID + "=?",
//                new String[]{String.valueOf(id)}, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        User user = new User(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2), cursor.getString(3));
//        return user;
//    }
//
//    // Getting All Data
//    public List<User> getAllSiswa() {
//        List<User> userList = new ArrayList<User>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_SISWA;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                User user = new User(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
//                        cursor.getString(2), cursor.getString(3));
//                userList.add(user);
//            } while (cursor.moveToNext());
//        }
//
//        return userList;
//    }
//
//    public int updateSiswa(User user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, user.get_name());
//        values.put(KEY_EMAIL, user.get_email());
//
//        // updating row
//        return db.update(TABLE_SISWA, values, KEY_ID + " = ?",
//                new String[]{String.valueOf(user.get_id())});
//    }
//
//    // Deleting single data
//    public void deleteSiswa(User user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_SISWA, KEY_ID + " = ?",
//                new String[]{String.valueOf(user.get_id())});
//        db.close();
//    }
//
//
//    // Getting Count
//    public int getSiswaCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_SISWA;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        cursor.close();
//
//        // return count
//        return cursor.getCount();
//    }

}
