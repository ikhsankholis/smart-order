package com.five_code.smartorder.data.remote;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

public class Interceptor implements okhttp3.Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder();

//        if (Preference.getLogin() == null) {
//            requestBuilder.addHeader("Authorization", "Basic NTk3ZTUxMmI4YjZiY2YxYTcwZDYzNDBmOm9nRnpYNUFoQ3RaTDFLSmNvT0RIdmZQTk9BVjlYSG1u");
//        }

        Request request = requestBuilder.build();

        return chain.proceed(request);
    }
}