package com.five_code.smartorder.data.model;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

public class BaseResponse {

    private MetaEntity meta;

    public MetaEntity getMeta() {
        return meta;
    }

    public static class MetaEntity {
        private int code;
        private String messages;
        private boolean status;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return messages;
        }

        public boolean isStatus() {
            return status;
        }
    }
}
