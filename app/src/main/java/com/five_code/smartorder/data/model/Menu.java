package com.five_code.smartorder.data.model;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class Menu {

    private String _id;
    private String nama;
    private String harga;
    private String deskripsi;
    private String stock;
    private int gambar;
    private KategoriIdBean kategori_id;
    private String createdAt;
    private String lastUpdate;
    private int quantity;
    private String note;

    public Menu(String nama, String harga, String deskripsi, int gambar) {
        this.nama = nama;
        this.harga = harga;
        this.deskripsi = deskripsi;
        this.gambar = gambar;
    }

    public String get_id() {
        return _id;
    }

    public String getNama() {
        return nama;
    }

    public String getHarga() {return harga; }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getStock() {
        return stock;
    }

    public int getGambar() {
        return gambar;
    }

    public KategoriIdBean getKategori_id() {
        return kategori_id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public static class KategoriIdBean {

        private String _id;
        private String kategori;
        private String createdAt;
        private String lastUpdate;

        public String get_id() {
            return _id;
        }

        public String getKategori() {
            return kategori;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getLastUpdate() {
            return lastUpdate;
        }
    }
}