package com.five_code.smartorder.data;

import com.five_code.smartorder.data.remote.AppService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

@Singleton
public class DataManager {

    private final AppService appService;

    @Inject
    public DataManager(AppService appService) {
        this.appService = appService;
    }

}
