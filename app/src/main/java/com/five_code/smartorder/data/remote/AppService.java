package com.five_code.smartorder.data.remote;

import com.five_code.smartorder.BuildConfig;
import com.five_code.smartorder.utils.RxErrorHandlingCallAdapterFactory;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

public interface AppService {

    String ENDPOINT = "";

    /******** Helper class that sets up a new services *******/
    class Creator {

        public static AppService newAppService() {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient().newBuilder();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);
            httpClientBuilder.addInterceptor(logging).build();

            httpClientBuilder.addInterceptor(new Interceptor());

            Retrofit retrofit = new Retrofit.Builder().baseUrl(AppService.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(
                            new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT,
                                    Modifier.STATIC).serializeNulls().create()))

                    .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                    .callFactory(httpClientBuilder.build())
                    .build();
            return retrofit.create(AppService.class);
        }
    }
}
